import os
import platform

from twisted.internet import defer

from . import data
from p2pool.util import math, pack, jsonrpc

nets = dict(
    wolfcoin=math.Object(
        P2P_PREFIX='fec3b9de'.decode('hex'),
        P2P_PORT=21777,
        ADDRESS_VERSION=73,
        RPC_PORT=21778, # default port is 9555
        RPC_CHECK=defer.inlineCallbacks(lambda bitcoind: defer.returnValue(
            'Wolfcoinaddress' in (yield bitcoind.rpc_help()) and
            not (yield bitcoind.rpc_getinfo())['testnet']
        )),
        SUBSIDY_FUNC=lambda height: (100*100000000) >> (height+1)//125000,
        BLOCKHASH_FUNC=lambda data: pack.IntType(256).unpack(__import__('xcoin_hash').getPoWHash(data)),
        POW_FUNC=lambda data: pack.IntType(256).unpack(__import__('xcoin_hash').getPoWHash(data)),
        BLOCK_PERIOD=300, # s
        SYMBOL='WLF',
        CONF_FILE_FUNC=lambda: os.path.join(os.path.join(os.environ['APPDATA'], 'Wolfcoin') if platform.system() == 'Windows' else os.path.expanduser('~/Library/Application Support/Wolfcoin/') if platform.system() == 'Darwin' else os.path.expanduser('~/.Wolfcoin'), 'Wolfcoin.conf'),
        BLOCK_EXPLORER_URL_PREFIX='',
        ADDRESS_EXPLORER_URL_PREFIX='',
        TX_EXPLORER_URL_PREFIX='',
        SANE_TARGET_RANGE=(2**256//2**32//1000 - 1, 2**256//2**22 - 1),
        DUMB_SCRYPT_DIFF=1,
        DUST_THRESHOLD=0.001e8,
    ),
)
for net_name, net in nets.iteritems():
    net.NAME = net_name
